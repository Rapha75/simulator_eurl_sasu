part of simulator_eurl_sasu;


ServiceRemuneration serviceRemuneration = new ServiceRemuneration();

class ServiceRemuneration {

  Logger logger = new Logger('simulator.ServiceRemuneration');

  Remuneration computeRemuneration(double remuneration, double calculBase) {

    List<ChargeEurlComputed> chargesComputedWithoutCsg = chargesEurl.charges.where((ChargeEurl chargeEurl) => !(chargeEurl is ChargeEurlCsgCrds))
                                                  .map((chargeEurl) => new ChargeEurlComputed(chargeEurl, chargeEurl.compute(calculBase))).toList();

    double sumChargesWithoutCsg = chargesComputedWithoutCsg.map((charge) => charge.chargeComputed).reduce((charge1, charge2) => charge1 + charge2);

    List<ChargeEurlComputed> chargesComputedCsg = chargesEurl.charges.where((ChargeEurl chargeEurl) => chargeEurl is ChargeEurlCsgCrds)
        .map((chargeEurl) => new ChargeEurlComputed(chargeEurl, chargeEurl.compute(calculBase + sumChargesWithoutCsg))).toList();

    return new Remuneration.computeFromCharges(remuneration, calculBase, new List.from(chargesComputedWithoutCsg)..addAll(chargesComputedCsg));
  }
}

