part of simulator_eurl_sasu;


class ChargeEurlThreshold extends ChargeEurl with Threshold {

  ChargeEurlThreshold(String name, double rate, double thresholdFrom, double thresholdTo) : super(name) {
    this.rate = rate;
    this.thresholdFrom = thresholdFrom;
    this.thresholdTo = thresholdTo;
  }
}

abstract class Threshold implements Compute {

  double rate;
  double thresholdFrom;
  double thresholdTo;

  double compute(double amount) {

    double amountToProcess = 0.00;

    if(amount >= thresholdFrom) {
      amountToProcess = amount - thresholdFrom;

      if(amount >= thresholdTo) {
        amountToProcess = thresholdTo - thresholdFrom;
      }
    }

    return amountToProcess * rate/100;
  }
}
