part of simulator_eurl_sasu;


class ChargeEurlCsgCrds extends ChargeEurlRate {

  bool isDeductible = false;

  ChargeEurlCsgCrds(String name, double rate, this.isDeductible) : super(name, rate);
}
