part of simulator_eurl_sasu;


class ChargeEurlRetraiteComplementaire extends ChargeEurl with RetraiteComplementaire {

  ChargeEurlRetraiteComplementaire(String name) : super(name);
}

abstract class RetraiteComplementaire implements Compute {

  double compute(double base) {

    if(base <= 26580) {
      return 1214.00;
    }

    if(base <= 49280) {
      return 2427.00;
    }

    if(base <= 57850) {
      return 3641.00;
    }

    if(base <= 66400) {
      return 6068.00;
    }

    if(base <= 83060) {
      return 8495.00;
    }

    if(base <= 103180) {
      return 13349.00;
    }

    if(base <= 123300) {
      return 14563.00;
    }

    return 15776.00;
  }
}
