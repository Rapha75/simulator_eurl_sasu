part of simulator_eurl_sasu;


class ChargeEurlFormationProfessionnelle extends ChargeEurl with FormationProfessionnelle {

  ChargeEurlFormationProfessionnelle(String name) : super(name);
}

abstract class FormationProfessionnelle implements Compute {

  double compute(double base) {
    return pass * 0.0025;
  }
}
