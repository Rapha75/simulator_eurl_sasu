part of simulator_eurl_sasu;


int pass = 41136;
int smic = 18473;

Charges charges = new Charges();

class Charges {

  List<ChargeSas> charges = [
    new ChargeSasCsgCrds("CSG non déductible", 2.40, 0.00, 0.9825, pass * 4.00, false),
    new ChargeSasCsgCrds("CSG déductible", 6.80, 0.00, 0.9825, pass * 4.00, true),
    new ChargeSasCsgCrds("CRDS non déductible", 0.50, 0.00, 0.9825, pass * 4.00, false),
    new ChargeSasLinear("Assurance Maladie", 0.00, 13.00), // LETESE SS DEPLAFONNEE
    new ChargeSasLinear("Contribution Solidarité Autonomie", 0.00, 0.30), // LETESE SS DEPLAFONNEE
    new ChargeSasThresholds("Retraite plafonnée", 6.90, 8.55, 0.00, pass * 1.00), // LETESE SS PLAFONNEE
    new ChargeSasLinear("Retraite déplafonnée", 0.40, 1.90), // LETESE SS DEPLAFONNEE
    new ChargeSasLinear("Allocations familiales à 3.45 %", 0.00, 3.45, chargeNullIfExceeds: 3.5 * smic), // LETESE SS DEPLAFONNEE - Le taux réduit (3,45 % au lieu de 5,25 %) à partir duquel est calculée cette cotisation lorsque le salaire est inférieur ou égal à 3,5 SMIC
    new ChargeSasLinear("Allocations familiales à 5.25 %", 0.00, 5.25, chargeNullIfBelow: 3.5 * smic), // LETESE SS DEPLAFONNEE - Le taux réduit (3,45 % au lieu de 5,25 %) à partir duquel est calculée cette cotisation lorsque le salaire est inférieur ou égal à 3,5 SMIC
    new ChargeSasThresholds("Aide au logement (FNAL)", 0.00, 0.10, 0.00, pass * 1.00),
    new ChargeSasLinear("Accident Travail", 0.00, 0.90), // LETESE SS DEPLAFONNEE
    new ChargeSasThresholds("Retraite Arrco TA", 3.15, 4.72, 0.00, pass * 1.00),
    new ChargeSasThresholds("Retraite Agirc TB", 8.64, 12.95, pass * 1.00, pass * 8.00),
    new ChargeSasThresholds("Retraite CEG TA", 0.86, 1.29, 0.00, pass * 1.00),
    new ChargeSasThresholds("Retraite CEG TB", 1.62, 1.08, pass * 1.00, pass * 8.00),
    new ChargeSasThresholds("CET", 0.14, 0.21, pass * 1.00, pass * 8.00),
    new ChargeSasThresholds("Prévoyance Cadre", 0.00, 1.50, 0.00, pass * 1.00, toAddToCsgCrdsBase: true),
    new ChargeSasThresholds("Prévoyance Cadre", 1.00, 1.15, pass * 1.00, pass * 2.00, toAddToCsgCrdsBase: true),
    new ChargeSasFixed("Mutuelle Santé", 0.0, 104.69 * 12, toAddToCsgCrdsBase: true),
    new ChargeSasThresholds("APEC", 0.024, 0.036, 0.00, pass * 4.00),
    new ChargeSasLinear("Formation", 0.00, 0.55),
    new ChargeSasLinear("Apprentissage", 0.00, 0.68),
    new ChargeSasThresholds("Assurance chômage", 0.00, 4.05, 0.00, pass * 4.00, toApplyToEmployeeOnly: true),
    new ChargeSasThresholds("AGS", 0.00, 0.15, 0.00, pass * 4.00, toApplyToEmployeeOnly: true)
  ];
}
